Für die Entwicklung der "Technik" (https://gitlab.com/ftcommunity/website-layout) 
speziell für die Darstellung des Bilderpools
auf einem "kleinen" Repo mit Testdaten. 
Damit können andere Testdaten, nämlich Bilder, als im Produktivdatenrepo
verwendet werden. 

**Anmerkung** Die ausgewählten Testbilder wurden aufgrund ihrer technischen Eigenschaften 
ausgewählt. Damit wird nichts über ihre Konstrukteure, Fotografen oder Uploader ausgesagt.
Die Zuordnung der Testbilder zu bestimmten Kategorien kann gegenüber dem Original abweichen.

Dieses Repo verwendet kein Submodul. 
Zum Testen musst Du es einfach auf Deinen Rechner klonen und
dort von Hugo die Seiten bauen lassen.  
**Achtung**: Gelegentlich scheint der Webserver von Hugo hier einen "Wackelkontakt"
zu haben. Es kann daher nötig sein, die Seiten nach `public` bauen zu lassen
und von dort mit einem anderen Webserver ausliefern zu lassen.