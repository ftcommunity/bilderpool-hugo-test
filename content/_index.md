---
Title: "Start"
ordersectionsby: "weight"
---

# ![fischertechnik community](images/logo-full.png)

Du baust gerne mit fischertechnik? Hier gibt es für dich ....

* einen [Bilderpool](./bilderpool) (schau dir Fotos anderer fischertechnik-Freunde an oder zeige ihnen deine eigenen Modelle)
* einen [Servicebereich](./knowhow) (lade Programme, Bauanleitungen und weitere nützliche Dinge herunter)
* ein [Forum](https://forum.ftcommunity.de/) (tritt in Kontakt mit Gleichgesinnten)
* die kostenlose Zeitschrift [ft:pedia](./ftpedia) (mit Artikeln und Modellvorstellungen)
* [Veranstaltungen](fans/veranstaltungen) (erlebe vor Ort, was mit fischertechnik alles möglich ist)
* eine [Teile-Datenbank](https://ft-datenbank.de/) (nutze die umfangreichen Informationen über aktuelle oder frühere Bauteile und Baukästen)


