---
layout: "image"
title: "Achterbahn"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim02.jpg"
weight: "2"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48250
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48250 -->
Von einem Mitarbeiter des Museums erbaut.