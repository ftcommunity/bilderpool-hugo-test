---
layout: "image"
title: "Achterbahn - Station"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim03.jpg"
weight: "3"
konstrukteure: 
- "Alexander Salameh (EMFT)"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48251
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48251 -->
In der Station läuft eine Kette mit Magneten darauf, die die Wagen zieht.