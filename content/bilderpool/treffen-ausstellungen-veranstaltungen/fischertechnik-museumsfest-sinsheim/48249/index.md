---
layout: "image"
title: "Bluetooth-Rennparcours"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48249
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48249 -->
