---
layout: "image"
title: "Achterbahn - Wagen"
date: "2018-10-22T12:52:12"
picture: "fischertechnikmuseumsfestsinsheim04.jpg"
weight: "4"
konstrukteure: 
- "Alexander Salameh"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/48252
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48252 -->
