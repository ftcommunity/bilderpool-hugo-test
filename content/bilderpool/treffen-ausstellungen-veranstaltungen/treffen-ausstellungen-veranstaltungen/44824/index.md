---
layout: "image"
title: "van Baal"
date: "2016-11-28T17:06:39"
picture: "modellschaumuenster48.jpg"
weight: "48"
konstrukteure: 
- "Rob van Baal"
fotografen:
- "-?-"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Higgsteilchen"
license: "unknown"
legacy_id:
- details/44824
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44824 -->
