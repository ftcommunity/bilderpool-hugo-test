---
layout: "image"
title: "Tim, Tom & Co machen Pause"
date: "2017-09-07T13:37:32"
picture: "Convention_klein.jpg"
weight: "0"
konstrukteure: 
- "Christian und Stefan"
fotografen:
- "Christian und Stefan"
keywords: ["Convention", "Südconvention", "2017"]
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46238
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46238 -->
Beim Bau der Modelle für die Convention legen Tim, Tom & Co erst mal eine Pause ein.