---
layout: "image"
title: "FT convention model"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen79.jpg"
weight: "79"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/48182
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48182 -->
FT-Convention 2018