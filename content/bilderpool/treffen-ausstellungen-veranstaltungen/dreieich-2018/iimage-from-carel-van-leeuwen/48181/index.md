---
layout: "image"
title: "FT convention model"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen78.jpg"
weight: "78"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/48181
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48181 -->
FT-Convention 2018