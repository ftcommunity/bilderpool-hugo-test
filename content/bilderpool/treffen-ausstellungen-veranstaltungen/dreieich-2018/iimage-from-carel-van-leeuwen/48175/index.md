---
layout: "image"
title: "FT convention model"
date: "2018-09-23T20:30:35"
picture: "dreieichimagefromcarelvanleeuwen72.jpg"
weight: "72"
konstrukteure: 
- "-?-"
fotografen:
- "Carel van Leeuwen"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "vleeuwen"
license: "unknown"
legacy_id:
- details/48175
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=48175 -->
FT-Convention 2018