---
layout: "image"
title: "Brickly-Uhr mit Glockenspiel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention061.jpg"
weight: "61"
konstrukteure: 
- "Peter Habermehl"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47980
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47980 -->
