---
layout: "image"
title: "Klingel"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention007.jpg"
weight: "7"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47926
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47926 -->
