---
layout: "image"
title: "Wankelmotoren, Motor"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention004.jpg"
weight: "4"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47923
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47923 -->
