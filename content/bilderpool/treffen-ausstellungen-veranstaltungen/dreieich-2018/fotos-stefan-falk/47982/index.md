---
layout: "image"
title: "Analog-Elektronik-Kleinfahrzeuge"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention063.jpg"
weight: "63"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47982
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47982 -->
Die Dinger fahren butterweich um Kurven. Da sitzen keine Computerdrin, sondern nur einfache Analogelektronik.