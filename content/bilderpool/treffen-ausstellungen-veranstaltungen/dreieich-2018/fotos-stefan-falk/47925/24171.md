---
layout: "comment"
hidden: true
title: "24171"
date: "2018-09-23T17:55:08"
uploadBy:
- "Rüdiger Riedel"
license: "unknown"
imported:
- "2019"
---
Der Synchronmotor der kleinen Zahnstangen-Uhr hat 10 Pole (Polpaarzahl 5) und dreht mit 600 U/min.