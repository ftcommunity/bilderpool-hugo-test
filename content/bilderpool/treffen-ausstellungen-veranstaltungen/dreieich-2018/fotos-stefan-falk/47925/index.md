---
layout: "image"
title: "Zahnstangenuhr"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention006.jpg"
weight: "6"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47925
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47925 -->
