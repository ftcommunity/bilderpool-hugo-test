---
layout: "image"
title: "Große Zahnraduhr und Wankelmotoren"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention003.jpg"
weight: "3"
konstrukteure: 
- "Rüdiger Riedel"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47922
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47922 -->
