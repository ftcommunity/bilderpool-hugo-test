---
layout: "image"
title: "Parcours"
date: "2018-09-23T16:50:30"
picture: "fischertechnikconvention064.jpg"
weight: "64"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47983
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47983 -->
