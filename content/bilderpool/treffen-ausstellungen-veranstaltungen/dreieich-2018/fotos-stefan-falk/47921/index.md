---
layout: "image"
title: "Rennwagen, 3D-Drucker"
date: "2018-09-23T16:50:29"
picture: "fischertechnikconvention002.jpg"
weight: "2"
konstrukteure: 
- "-?-"
fotografen:
- "Stefan Falk"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Stefan Falk"
license: "unknown"
legacy_id:
- details/47921
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=47921 -->
