---
layout: "image"
title: "Schwebebahnstrecke 1"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt01.jpg"
weight: "1"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Depot"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44498
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44498 -->
An diesem Ende der Strecke gibt es geballte Technik. Das Depot mit Weichen und Abstellgleisen aus grauen Teilen gebaut, und die hiesige Endstation in gelb. Eine Kehrschleife umrundet das Depot.