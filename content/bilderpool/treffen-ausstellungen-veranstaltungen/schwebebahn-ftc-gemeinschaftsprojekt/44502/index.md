---
layout: "image"
title: "Schwebebahnstrecke 5"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt05.jpg"
weight: "5"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44502
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44502 -->
Die Kurve.