---
layout: "image"
title: "Schwebebahnstrecke 4"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt04.jpg"
weight: "4"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Station"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44501
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44501 -->
Ein Haltepunkt mit Aufzügen.