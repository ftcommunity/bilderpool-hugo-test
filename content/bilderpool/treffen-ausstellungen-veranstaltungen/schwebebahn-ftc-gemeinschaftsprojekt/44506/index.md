---
layout: "image"
title: "Schwebebahnstrecke 9"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt09.jpg"
weight: "9"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Zugwendeanlage", "Drehscheibe"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44506
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44506 -->
An diesem Streckenende befindet sich die Zugwendeanlage. Eine Drehscheibe wendet den angekommenen Zug und befördert ihn dabei auf das andere Gleis.