---
layout: "image"
title: "Depot - Detail der Weichenveriegelung 1"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt10.jpg"
weight: "10"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Depot"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44507
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44507 -->
So werden die Weichen exakt positioniert. Ein speziell gefertigter Trichter fängt die Schubstange ein und führt den Schienenträger zur Sollposition.