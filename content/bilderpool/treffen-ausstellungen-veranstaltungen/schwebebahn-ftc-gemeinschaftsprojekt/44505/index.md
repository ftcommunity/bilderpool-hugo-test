---
layout: "image"
title: "Schwebebahnstrecke 8"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt08.jpg"
weight: "8"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Station"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44505
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44505 -->
Endstation - Bitte alle Aussteigen