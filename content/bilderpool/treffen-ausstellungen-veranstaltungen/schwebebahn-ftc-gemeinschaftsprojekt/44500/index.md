---
layout: "image"
title: "Schwebebahnstrecke 3"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt03.jpg"
weight: "3"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44500
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44500 -->
Hier hat sich eine nicht so filigrane graue Stütze dazwischengemogelt.