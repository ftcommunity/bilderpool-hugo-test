---
layout: "image"
title: "Depot - Detail der Weichenveriegelung 2"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt11.jpg"
weight: "11"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn", "Depot"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44508
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44508 -->
Der Antrieb der Schubstange erfolgt pragmatisch mit Pneumatikzylinder.