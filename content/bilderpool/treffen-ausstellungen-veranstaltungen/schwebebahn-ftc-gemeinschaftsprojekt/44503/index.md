---
layout: "image"
title: "Schwebebahnstrecke 6"
date: "2016-10-02T17:43:47"
picture: "schwebebahnftcgemeinschaftsprojekt06.jpg"
weight: "6"
konstrukteure: 
- "-?-"
fotografen:
- "R. Trapp"
keywords: ["Schwebebahn"]
uploadBy: "H.A.R.R.Y."
license: "unknown"
legacy_id:
- details/44503
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=44503 -->
Wieder ein Stück Strecke.