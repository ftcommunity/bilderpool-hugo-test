---
layout: "image"
title: "Schoko-BS30 (4)"
date: "2013-04-02T15:32:14"
picture: "P1070854_verkleinert.jpg"
weight: "0"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36822
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36822 -->
Nach dem (äußerst vorsichtigen!) Herausziehen des Verbinders liegt die erste Nut in der Schokolade frei.