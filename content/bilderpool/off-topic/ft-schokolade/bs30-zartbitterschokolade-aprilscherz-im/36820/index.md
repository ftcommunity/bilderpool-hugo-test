---
layout: "image"
title: "Schoko-BS30 (2)"
date: "2013-04-02T15:32:14"
picture: "P1070852_verkleinert.jpg"
weight: "0"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36820
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36820 -->
Ohne "Deckel" - statt dem gewünschten Zapfen eine Luftblase.