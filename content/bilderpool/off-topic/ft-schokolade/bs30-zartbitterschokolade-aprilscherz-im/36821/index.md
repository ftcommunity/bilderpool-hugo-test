---
layout: "image"
title: "Schoko-BS30 (3)"
date: "2013-04-02T15:32:14"
picture: "P1070853_verkleinert.jpg"
weight: "0"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36821
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36821 -->
Ohne Boden.