---
layout: "image"
title: "Schoko-BS30 (6)"
date: "2013-04-02T15:32:14"
picture: "P1070859_verkleinert.jpg"
weight: "0"
konstrukteure: 
- "bflehner"
fotografen:
- "bflehner"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "Bernhard Lehner"
license: "unknown"
legacy_id:
- details/36824
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=36824 -->
Und schließlich der komplett befreite und mit einem Messer leicht nachbearbeitete BS30, wie er im Forum veröffentlicht wurde. 
Tatsächlich besteht er aus vielen kleinen Stückchen, die für dieses Bild zusammengepuzzelt wurden. Bei genauem Hinsehen ist auch unten links im Bild eine Lücke eines weggebrochenen Teils erkennbar.