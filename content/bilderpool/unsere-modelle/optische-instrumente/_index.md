---
layout: "image-collection"
title: "Optische Instrumente"
date: 2019-10-25T21:04:06+02:00
legacy_id:
- categories/3517
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3517 --> 
Hierher packen wir Mikroskope, Teleskope, und was sonst noch alles mit Optik zu tun hat.