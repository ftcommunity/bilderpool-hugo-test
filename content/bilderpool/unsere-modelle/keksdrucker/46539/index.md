---
layout: "image"
title: "Reedkontakt"
date: "2017-09-30T11:52:18"
picture: "keksdrucker09.jpg"
weight: "9"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46539
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46539 -->
Die Positionen im Trockenlager (Reihe und Position) werden mit wartungsarmen Reedkontakten erfasst. Neodymmagneten in einem 4mm Schlauch sind preiswert und platzsparend. In den Endpunkten der Strecken werden Referenztaster eingesetzt.