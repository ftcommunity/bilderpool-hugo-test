---
layout: "image"
title: "Keksdrucker"
date: "2017-09-30T11:52:18"
picture: "keksdrucker01.jpg"
weight: "1"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46531
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46531 -->
Totalansicht des Druckers. Wer es einfacher mag, kann den Drucker rein auf das Druckwerk reduzieren und lässt Kekszuführung und Trockenlager weg.