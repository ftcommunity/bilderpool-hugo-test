---
layout: "comment"
hidden: true
title: "23798"
date: "2017-11-07T21:52:10"
uploadBy:
- "ftpi"
license: "unknown"
imported:
- "2019"
---
Das "Mainboard" ist ein normaler TXT. Daran hängt via I2C ein PWM-Board mit Leistungstreibern um genügend Motorausgänge zu haben (z.B. ftpedia 2/16 ftPI). Die Software läuft zu 100% in RoboPro.

Für die Mechanik schiebt ein Pneumatikzylinder den untersten Keks aus einem Stapel auf ein normales Förderband. Mit einer Linsenlampe und Fototransistor wird der Keks am Druckkopf erkannt und mit einem zweiten Pneumatikzylinder festgehalten.

Mit zwei Encodermotoren wird der Druckkopf in XY-Richtung verfahren. Mit zwei Referenztastern wird die Null-Position gefunden, danach mit Encodesignalen gefahren.

Der Hubkolben der Spritzen wird mit einem Spindelantrieb zugefahren. Hier haben wir auf Referenz/Überfahrtaster verzichtet. Wenn kurz vor leer muss manuell die Sprotze gewechselt werden.

Der Weitertransport nach dem Druck über das Förderband wird rein über die Zeit gesteuert (n Sekunden Band an).

Die Positionen im Trockenlager werden über zwei Reedkontakte gemessen. Auch hier wird teilweise über n Sekunden Motor an agiert um weniger Sensoren zu brauchen.