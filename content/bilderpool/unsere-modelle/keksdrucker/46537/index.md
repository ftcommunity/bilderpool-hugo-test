---
layout: "image"
title: "Greifer"
date: "2017-09-30T11:52:18"
picture: "keksdrucker07.jpg"
weight: "7"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46537
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46537 -->
Der Greifer ist möglichst leicht gebaut, um den Horizontalantrieb mit einem Hubgetriebe auf einer Zahnstange laufen zu lassen. Der Antrieb hat nur in eine Richtung einen Referenztaster, die andere Richtung wird über die Fahrzeit geregelt.
