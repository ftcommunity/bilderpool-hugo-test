---
layout: "image"
title: "Vertikalantrieb"
date: "2017-09-30T11:52:18"
picture: "keksdrucker08.jpg"
weight: "8"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46538
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46538 -->
Mit zwei verbundenen Hubgetrieben ist der Vertikalantrieb auf Zahnstangen präzise genug. Das zweite Getriebe ist nicht angetrieben - es kann auch ein defektes Getriebe verwendet werden.