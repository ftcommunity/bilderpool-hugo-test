---
layout: "image"
title: "Auslagern"
date: "2017-09-30T11:52:18"
picture: "keksdrucker12.jpg"
weight: "12"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46542
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46542 -->
keks im Trockenlager abholen, in die zweite Reihe oberhalb des Auswurfs fahren und nach unten fahren bis der Keks vom Greifer rutscht.