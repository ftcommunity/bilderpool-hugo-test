---
layout: "image"
title: "Trockenlager"
date: "2017-09-30T11:52:18"
picture: "keksdrucker10.jpg"
weight: "10"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46540
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46540 -->
Im Trockenlager sind die Kekse solange, bis der Zuckerguß hart geworden ist. Für die Steuerung ist die Belegung des Lagers egal. Vor dem Einlagern eines neuen Keks in eine Lagerposition wird zunächst die Postion geräumt - ob nun ein Keks in der Position ist oder nicht.