---
layout: "image-collection"
title: "Keksdrucker"
date: 2019-10-25T21:04:05+02:00
legacy_id:
- categories/3438
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/categories.php?cat_id=3438 --> 
Weihnachten steht vor der Tür. Obwohl 3D Drucker schon in vielen fischertechnikerhaushalten stehen, müssen Kekse immer noch manuell verziert werden. Hier hilft Magic's Cookie Printer - die Fotos zeigen wie er funktioniert.