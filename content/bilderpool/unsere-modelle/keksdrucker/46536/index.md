---
layout: "image"
title: "Bandantrieb"
date: "2017-09-30T11:52:18"
picture: "keksdrucker06.jpg"
weight: "6"
konstrukteure: 
- "Fabian, Max, Christian & Stefan"
fotografen:
- "Christian & Stefan"
keywords: "KEYWORDS OPTIONAL"
uploadBy: "ftpi"
license: "unknown"
legacy_id:
- details/46536
imported:
- "2019"
---

<!-- https://www.ftcommunity.de/details.php?image_id=46536 -->
Ein M-Motor zieht das Förderband. Es wird in einer Nut zwischen zwei Winkelträger geführt. Achtung: Die Kette nicht zu straff wählen!