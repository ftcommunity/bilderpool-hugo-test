---
hidden: true
layout: "file"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
date: {{ .Date }}
file: "ftpedia-{{ .Name }}.pdf"
uploadBy:
- "ft:pedia-Redaktion"
---

