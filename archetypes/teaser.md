---
hidden: true
layout: "teaser"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
publ: {{ .Date }}
date: {{ .Date }}
---

